package com.bartslota.provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.micrometer.tracing.Tracer;
import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/provider")
class ProviderController {

    private static final Logger LOG = LoggerFactory.getLogger(ProviderController.class);

    private final Tracer tracer;

    ProviderController(Tracer tracer) {
        this.tracer = tracer;
    }

    @GetMapping
    String get(HttpServletRequest req) {
        LOG.info("Some request accepted");
        req.getHeaderNames().asIterator().forEachRemaining(it -> LOG.info("Header: {} -> {}", it, req.getHeader(it)));
        return "Some response";
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    void register(@RequestBody User user) {
        //TODO: read baggage
        LOG.info("User registered: {}", user);
    }

}
