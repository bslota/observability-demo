package com.bartslota.provider.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;

import static com.bartslota.provider.kafka.KafkaConfigurationBuilder.configFor;

@EnableKafka
@Configuration
public class KafkaConfigurationProvider {

    private final String defaultBrokers;

    KafkaConfigurationProvider(@Value("${spring.kafka.bootstrap-servers}") String kafkaBrokers) {
        this.defaultBrokers = kafkaBrokers;
    }

    public <T> KafkaConfigurationBuilder<T> defaultConfigFor(Class<T> messageType) {
        return configFor(messageType).brokers(defaultBrokers);
    }
}
