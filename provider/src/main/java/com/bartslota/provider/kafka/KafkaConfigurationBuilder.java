package com.bartslota.provider.kafka;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.CommonErrorHandler;
import org.springframework.kafka.support.mapping.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.mapping.Jackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.ParseStringDeserializer;

import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import static com.bartslota.provider.kafka.RetryingKafkaErrorHandler.retryingErrorHandler;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;

public final class KafkaConfigurationBuilder<T> {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaConfigurationBuilder.class);

    private String brokers;
    private Integer concurrency;
    private String groupId;
    private String autoOffsetReset = "earliest";
    private CommonErrorHandler errorHandler = retryingErrorHandler().ignoring(IllegalArgumentException.class);
    private Class<T> messageType;
    private Duration idleEventInterval;
    private final Map<String, Class<?>> idClassMappings = new HashMap<>();

    public static <T> KafkaConfigurationBuilder<T> configFor(Class<T> messageType) {
        KafkaConfigurationBuilder<T> config = new KafkaConfigurationBuilder<>();
        config.messageType = messageType;
        return config;
    }

    public KafkaConfigurationBuilder<T> brokers(String value) {
        this.brokers = value;
        return this;
    }

    public KafkaConfigurationBuilder<T> groupId(String value) {
        this.groupId = value;
        return this;
    }

    public KafkaConfigurationBuilder<T> autoOffsetReset(String value) {
        this.autoOffsetReset = value;
        return this;
    }

    public KafkaConfigurationBuilder<T> errorHandler(RetryingKafkaErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
        return this;
    }

    public KafkaConfigurationBuilder<T> idClassMapping(String messageType, Class<?> type) {
        this.idClassMappings.put(messageType, type);
        return this;
    }

    public KafkaConfigurationBuilder<T> idleEventInterval(Duration idleEventInterval) {
        this.idleEventInterval = idleEventInterval;
        return this;
    }

    public ConcurrentKafkaListenerContainerFactory<String, T> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, T> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.getContainerProperties().setObservationEnabled(true);
        factory.setConsumerFactory(eventConsumerFactory());
        factory.setCommonErrorHandler(errorHandler);
        factory.setRecordInterceptor((consumerRecord, consumer) -> {
            Optional.ofNullable(consumerRecord.value())
                    .ifPresent(event -> LOG.debug("Processing message with key: {}, value: {}", consumerRecord.key(), event));
            return consumerRecord;
        });

        if (concurrency != null) {
            factory.setConcurrency(concurrency);

            return factory;
        }

        return factory;
    }

    public ProducerFactory<String, String> kafkaEventProducer() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
        configs.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configs.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return new DefaultKafkaProducerFactory<>(configs);
    }

    private ConsumerFactory<String, T> eventConsumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);
        props.put(ConsumerConfig.ALLOW_AUTO_CREATE_TOPICS_CONFIG, false);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        props.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_committed");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

        return new DefaultKafkaConsumerFactory<>(props);
    }

}
