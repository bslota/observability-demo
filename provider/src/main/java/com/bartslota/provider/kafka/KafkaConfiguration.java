package com.bartslota.provider.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;

import com.bartslota.provider.User;

@Configuration
class KafkaConfiguration {

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String> userContainerFactory(
            KafkaConfigurationProvider configurationProvider) {
        return configurationProvider.defaultConfigFor(String.class)
                                    .groupId("my-group")
                                    .autoOffsetReset("earliest")
                                    .kafkaListenerContainerFactory();
    }

}
