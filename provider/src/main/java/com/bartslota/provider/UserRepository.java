package com.bartslota.provider;

import org.springframework.data.repository.CrudRepository;

/**
 * @author bslota on 21/02/2023
 */
interface UserRepository extends CrudRepository<UserEntity, String> {

}
