package com.bartslota.provider;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
@KafkaListener(topics = "observation-topic", containerFactory = "userContainerFactory")
class UserListener {

    private static final Logger LOG = LoggerFactory.getLogger(UserListener.class);

    private final UserRepository userRepository;

    private final ObjectMapper objectMapper;

    UserListener(UserRepository userRepository, ObjectMapper objectMapper) {
        this.userRepository = userRepository;
        this.objectMapper = objectMapper;
    }

    @KafkaHandler(isDefault = true)
    void handle(ConsumerRecord<String, String> record) {
        LOG.info("Record consumed: {} -> {}", record.key(), record.value());
        LOG.info("Headers: ");
        record.headers().forEach(it -> LOG.info("Header {} -> {}", it.key(), new String(it.value())));
        User user = userFrom(record);
        UserEntity userEntity = userRepository.findById(user.id()).orElse(new UserEntity(user.id(), user.type()));
        userRepository.save(userEntity);
    }

    private User userFrom(ConsumerRecord<String, String> record) {
        try {
            return objectMapper.readValue(record.value(), User.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
