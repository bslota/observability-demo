package com.bartslota.provider;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * @author bslota on 21/02/2023
 */
@Entity
@Table(name = "user_details")
class UserEntity {

    @Id
    private String userId;

    private String userType;

    protected UserEntity() {
    }

    UserEntity(String userId, String userType) {
        this.userId = userId;
        this.userType = userType;
    }
}
