package com.bartslota.client

import io.micrometer.observation.tck.TestObservationRegistry
//import io.micrometer.tracing.test.simple.SimpleTracer
import io.restassured.RestAssured
import io.restassured.response.Response
import org.springframework.boot.test.autoconfigure.actuate.observability.AutoConfigureObservability
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.context.annotation.Bean
import spock.lang.Specification

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE

/**
 * @author bslota on 28/02/2023
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureObservability
class ClientControllerSpec extends Specification {

    @LocalServerPort
    int port

    def setup() {
        RestAssured.baseURI = "http://localhost:" + port
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails()
    }


    def "should successfully register user with phone number"() {
        when:
            Response response = RestAssured.given()
                    .get("/api/client")

        then:
            response.statusCode == 200
    }

    @TestConfiguration
    class ObservationTestConfiguration {

        @Bean
        TestObservationRegistry observationRegistry() {
            return TestObservationRegistry.create();
        }
//
//        @Bean
//        SimpleTracer simpleTracer() {
//            return new SimpleTracer();
//        }
    }
}
