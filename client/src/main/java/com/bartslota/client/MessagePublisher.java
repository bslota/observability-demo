package com.bartslota.client;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.IntStream;

import org.springframework.stereotype.Component;

@Component
class MessagePublisher {

    private LinkedBlockingQueue<Message> messages = new LinkedBlockingQueue<>();

    void publish(Message msg) {
        messages.add(msg);
    }

    List<Message> get(int batchSize) {
        List<Message> foundMessages = new LinkedList<>();
        IntStream.range(0, batchSize).forEach(it -> {
            Message peek = messages.poll();
            if (peek != null) {
                foundMessages.add(peek);
            }
        });
        return foundMessages;
    }

    record Message(String key, String content, String channel, String traceId, String spanId, Map<String, String> baggage) {

    }

}
