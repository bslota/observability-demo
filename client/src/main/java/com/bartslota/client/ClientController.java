package com.bartslota.client;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.micrometer.observation.Observation;
import io.micrometer.observation.ObservationRegistry;
import io.micrometer.observation.annotation.Observed;
import io.micrometer.tracing.TraceContext;
import io.micrometer.tracing.Tracer;

@RestController
@RequestMapping("/api/client")
class ClientController {

    private static final Logger LOG = LoggerFactory.getLogger(ClientController.class);

    private final ProviderFacade providerFacade;
    private final RestTemplate restTemplate;

    private final KafkaTemplate<String, String> kafkaTemplate;

    private final ObservationRegistry observationRegistry;

    private final ObjectMapper objectMapper;

    private final Tracer tracer;

    private final MessagePublisher messagePublisher;

    public ClientController(ProviderFacade providerFacade, RestTemplate restTemplate, KafkaTemplate<String, String> kafkaTemplate, ObservationRegistry observationRegistry, ObjectMapper objectMapper, Tracer tracer, MessagePublisher messagePublisher) {
        this.providerFacade = providerFacade;
        this.restTemplate = restTemplate;
        this.kafkaTemplate = kafkaTemplate;
        this.observationRegistry = observationRegistry;
        this.objectMapper = objectMapper;
        this.tracer = tracer;
        this.messagePublisher = messagePublisher;
    }

    @GetMapping
    @Observed
    String getFromProvider() {

        LOG.info("Some request accepted");
        // return providerFacade.get();
        return restTemplate.getForObject("http://localhost:8082/api/provider", String.class);
    }

    @PostMapping(path = "/kafka", consumes = MediaType.APPLICATION_JSON_VALUE)
    void sendToKafka(@RequestBody User user) {
        // observationRegistry.getCurrentObservation().getContext().put("some", "value");
        Observation observation = Observation.createNotStarted("user.registration", observationRegistry)
                                             .contextualName("register-user")
                                             // .observationConvention(new ObservabilityConfiguration.MyConvention())
                                             .lowCardinalityKeyValue("user.type", user.type)
                                             .highCardinalityKeyValue("user.id", user.id);
        observation
                .observe(() -> {
                    // return restTemplate.getForObject("http://localhost:8082/api/provider", String.class);
                    tracer.createBaggage("user.id", user.id);
                    LOG.info("User registered: {}", user);
                    var message = new ProducerRecord<>("observation-topic", user.id, asJson(user));
                    message.headers().add(new RecordHeader("custom-header", "bla bla".getBytes()));
                    kafkaTemplate.send(message);
                });

        //TODO: uzupełnić MDC o user.id
    }

    @PostMapping(path = "/outbox", consumes = MediaType.APPLICATION_JSON_VALUE)
    void sendToOutbox(@RequestBody User user) {
        // observationRegistry.getCurrentObservation().getContext().put("some", "value");
        Observation observation = Observation.createNotStarted("user.registration", observationRegistry)
                                             .contextualName("register-user")
                                             // .observationConvention(new ObservabilityConfiguration.MyConvention())
                                             .lowCardinalityKeyValue("user.type", user.type)
                                             .highCardinalityKeyValue("user.id", user.id);
        observation
                .observe(() -> {
                    // return restTemplate.getForObject("http://localhost:8082/api/provider", String.class);
                    tracer.createBaggage("user.id", user.id);
                    LOG.info("User registered: {}", user);
                    TraceContext context = tracer.currentTraceContext().context();
                    messagePublisher.publish(new MessagePublisher.Message(user.id, asJson(user), "observation-topic", context.traceId(), context.spanId(), tracer.getAllBaggage()));
                });
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    String register(@RequestBody User user) {
        Observation observation = Observation.createNotStarted("user.registration", observationRegistry)
                                             .contextualName("register-user")
                                             .observationConvention(new ObservabilityConfiguration.MyConvention())
                                             .lowCardinalityKeyValue("user.type", user.type)
                                             .highCardinalityKeyValue("user.id", user.id);
        return observation
                .observe(() -> {
                    LOG.info("User registered: {}", user);
                    // return restTemplate.getForObject("http://localhost:8082/api/provider", String.class);
                    tracer.createBaggage("user.id", user.id);
                    return providerFacade.get();
                });
    }



    record User(String id, String type) {

    }

    private String asJson(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException ex) {
            throw new RuntimeException(ex);
        }
    }
}
