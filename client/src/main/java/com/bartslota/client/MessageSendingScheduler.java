package com.bartslota.client;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import io.micrometer.observation.Observation;
import io.micrometer.observation.ObservationRegistry;
import io.micrometer.tracing.CurrentTraceContext;
import io.micrometer.tracing.TraceContext;
import io.micrometer.tracing.Tracer;
import io.micrometer.tracing.otel.bridge.OtelTraceContextBuilder;

/**
 * @author bslota on 20/02/2023
 */
@Component
class MessageSendingScheduler {

    private static final Logger LOG = LoggerFactory.getLogger(MessageSendingScheduler.class);

    private final MessagePublisher messagePublisher;

    private final KafkaTemplate<String, String> kafkaTemplate;

    private final Tracer tracer;

    private final ObservationRegistry observationRegistry;

    MessageSendingScheduler(MessagePublisher messagePublisher, KafkaTemplate<String, String> kafkaTemplate, Tracer tracer, ObservationRegistry observationRegistry) {
        this.messagePublisher = messagePublisher;
        this.kafkaTemplate = kafkaTemplate;
        this.tracer = tracer;
        this.observationRegistry = observationRegistry;
    }

    @Scheduled(fixedDelay = 500)
    void send() {
        messagePublisher.get(10).forEach(msg -> {

            var message = new ProducerRecord<>(msg.channel(), msg.key(), msg.content());

            LOG.info("BEFORE handling message");
            TraceContext newContext = new OtelTraceContextBuilder()
                    .traceId(msg.traceId())
                    .sampled(true)
                    .parentId(msg.traceId())
                    .spanId(msg.spanId())
                    .build();
            try (CurrentTraceContext.Scope scope = tracer.currentTraceContext().newScope(newContext)) {
                // Observation.createNotStarted("outbox", observationRegistry)
                //            .contextualName("outbox scheduler")
                //            .highCardinalityKeyValue("user.id", msg.key())
                //            .observe(() -> {
                               // tracer.startScopedSpan("handling message");

                               msg.baggage().forEach(tracer::createBaggage);
                               LOG.info("Sending message: {}", message);
                               kafkaTemplate.send(message);
                           // });
            }
            LOG.info("AFTER handling message");
        });

    }
}
