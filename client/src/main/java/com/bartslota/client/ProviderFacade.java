package com.bartslota.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "providerFacade", url = "localhost:8082/api/provider")
interface ProviderFacade {

    @GetMapping
    String get();
}
