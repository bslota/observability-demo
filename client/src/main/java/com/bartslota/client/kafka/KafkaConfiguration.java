package com.bartslota.client.kafka;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

@Configuration
class KafkaConfiguration {

    @Bean
    KafkaTemplate<String, String> kafkaTemplate(ProducerFactory<String, String> kafkaEventProducer) {
        KafkaTemplate<String, String> kafkaTemplate = new KafkaTemplate<>(kafkaEventProducer);
        kafkaTemplate.setObservationEnabled(true);
        return kafkaTemplate;
    }

    @Bean
    ProducerFactory<String, String> kafkaEventProducer(KafkaConfigurationProvider configurationProvider) {
        return configurationProvider.defaultConfigFor(String.class).kafkaEventProducer();
    }

}
