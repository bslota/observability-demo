package com.bartslota.client.kafka;

import org.springframework.kafka.listener.DefaultErrorHandler;
import org.springframework.util.backoff.FixedBackOff;

import static org.springframework.util.backoff.FixedBackOff.UNLIMITED_ATTEMPTS;

public final class RetryingKafkaErrorHandler extends DefaultErrorHandler {

    private static final long BACK_OFF_INTERVAL = 500L;

    private RetryingKafkaErrorHandler() {
        super(new FixedBackOff(BACK_OFF_INTERVAL, UNLIMITED_ATTEMPTS));
    }

    public static RetryingKafkaErrorHandler retryingErrorHandler() {
        return new RetryingKafkaErrorHandler();
    }

    public RetryingKafkaErrorHandler ignoring(Class<? extends RuntimeException> exception) {
        this.addNotRetryableExceptions(exception);
        return this;
    }

}
