package com.bartslota.client;

import org.springframework.boot.actuate.web.exchanges.HttpExchangeRepository;
import org.springframework.boot.actuate.web.exchanges.InMemoryHttpExchangeRepository;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import feign.micrometer.DefaultFeignObservationConvention;
import feign.micrometer.FeignContext;
import feign.micrometer.MicrometerObservationCapability;
import io.micrometer.common.KeyValue;
import io.micrometer.common.KeyValues;
import io.micrometer.observation.ObservationRegistry;
import io.micrometer.observation.aop.ObservedAspect;

@Configuration
class ObservabilityConfiguration {

    @Bean
    ObservedAspect observedAspect(ObservationRegistry observationRegistry) {
        return new ObservedAspect(observationRegistry);
    }

    @Bean
    public HttpExchangeRepository httpTraceRepository() {
        return new InMemoryHttpExchangeRepository();
    }

    @Bean
    RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    MicrometerObservationCapability micrometerObservationCapability(ObservationRegistry observationRegistry) {
        return new MicrometerObservationCapability(observationRegistry, new MyConvention());
    }

    public static class MyConvention extends DefaultFeignObservationConvention {

        @Override
        public KeyValues getHighCardinalityKeyValues(FeignContext context) {
            // here, we just want to have an additional KeyValue to the observation, keeping the default values
            return super.getHighCardinalityKeyValues(context).and(custom(context));
        }

        private KeyValue custom(FeignContext context) {
            return KeyValue.of("user.id", null);
        }

    }
}
