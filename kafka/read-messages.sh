docker exec --interactive --tty broker \
kafka-console-consumer --bootstrap-server broker:9092 \
                       --topic observation-topic \
                       --from-beginning \
                       --property print.key=true \
                       --property print.headers=true